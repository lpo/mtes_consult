import scrapy


class MtesCrawlSpider(scrapy.Spider):
    name = "mtes_crawl"
    allowed_domains = [
        "http://www.consultations-publiques.developpement-durable.gouv.fr/"
    ]
    start_urls = [
        "http://www.consultations-publiques.developpement-durable.gouv.fr/projet-d-arrete-modifiant-les-conditions-d-a2106.html"
    ]

    page = 0
    max_pages = 1

    def parse(self, response):
        for ligne in response.css("div.ligne-com"):
            yield {
                "sujet": ligne.css("div.titresujet::text").getall(),
                # "texte": ligne.css("div.textesujet")
            }

        if self.page < self.max_pages:
            self.page += 1
            yield scrapy.Request(
                self.start_urls[0] + "?debut_forums=20",
                callback=self.parse,
                dont_filter=True,
            )

